<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\User;
use App\Http\Requests;

class EmailController extends Controller
{
  /**
   * Send an e-mail reminder to the user.
   *
   * @param  Request  $request
   * @param  int  $id
   * @return Response
   */

    public function sendEmail(Request $request, $id)
    {
        $user = User::findOrfail($id);
        Mail::send('emails.reminder', ['user' => $user], function ($m) use ($user) {
            $m->from('hello@app.com', 'Your Application');

            $m->to($user->email, $user->name)->subject('Your Reminder!');
        });
    }
}
