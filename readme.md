# Purpose #
 Configuration of email with Laravel out of the box.

## Pre-Requisites ##
* Laravel
* Gmail Account

### Important Notice ###
 
Less secure apps access should be turned *ON* in Gmail using this link :
https://www.google.com/settings/security/lesssecureapps
Otherwise, error will be thrown.
